﻿//
// ************************************************************************
// Copyright (c) 2016 Hydro-Technologies All rights reserved
// This source file is the exclusive property of Hydro-Technologies and may
// not be copied or used without prior written permission.
// ************************************************************************
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace HT_VLP16_Lib {
    public class Recorder {

        public DateTime GetCnxTime() { return m_cnxTime; }
        public UInt32 GetDataPacketCount() { return m_dataPacketCount; }
        public UInt32 GetGPSPacketCount() { return m_gpsPacketCount; }

        private DateTime m_cnxTime = DateTime.Now;

        public const int kDATA_PACKET_SIZE = 1206;
        public const int kUDP_DATA_PORT = 2368;
        private UInt32 m_dataPacketCount = 0;
        private UdpClient m_udpDataClient = null;
        //private FileStream m_fsDataOut = null;

        public const int kGPS_PACKET_SIZE = 512;
        public const int kUDP_GPS_PORT = 8308;
        private UInt32 m_gpsPacketCount = 0;
        private UdpClient m_udpGpsClient = null;
        //private FileStream m_fsGpsOut = null;

        public delegate void DataCallbackType(Lidar.DataPacket packet);
        private DataCallbackType m_dataCallback;

        public delegate void GPSCallbackType(GPS.DataPacket packet);
        private GPSCallbackType m_gpsCallback;
        private bool m_recording = false;


        // **********************************************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="dataCallback"></param>
        /// <param name="gpsCallback"></param>
        public void StartRecord(DataCallbackType dataCallback = null, GPSCallbackType gpsCallback = null) {
            try {
                m_dataCallback = dataCallback;
                m_gpsCallback = gpsCallback;

                //int fileNum = 0;
                //string dataFilename = "";
                //string gpsFilename = "";
                //do {
                //    dataFilename = directory.FullName + "//LidarData" + fileNum + ".bin";
                //    gpsFilename = directory.FullName + "//GPSData" + fileNum + ".bin";
                //    fileNum++;
                //} while (File.Exists(dataFilename));
                //m_fsDataOut = new FileStream(dataFilename, FileMode.Create);
                m_dataPacketCount = 0;
                m_udpDataClient = new UdpClient(kUDP_DATA_PORT);
                m_udpDataClient.BeginReceive(new AsyncCallback(RxDataCallback), null);

                if (gpsCallback != null) {
                    //m_fsGpsOut = new FileStream(gpsFilename, FileMode.Create);
                    m_gpsPacketCount = 0;
                    m_udpGpsClient = new UdpClient(kUDP_GPS_PORT);
                    m_udpGpsClient.BeginReceive(new AsyncCallback(RxGpsCallback), null);
                }

                m_cnxTime = DateTime.Now;
                m_recording = true;
            } catch { }
        }

        // **********************************************************
        public void StopRecord() {
            if (m_udpDataClient != null) {
                m_udpDataClient.Close();
                m_udpDataClient = null;
            }
            //if (m_fsDataOut != null) {
            //    m_fsDataOut.Close();
            //    m_fsDataOut = null;
            //}

            if (m_udpGpsClient != null) {
                m_udpGpsClient.Close();
                m_udpGpsClient = null;
            }
            //if (m_fsGpsOut != null) {
            //    m_fsGpsOut.Close();
            //    m_fsGpsOut = null;
            //}
            m_recording = false;
        }

        // **********************************************************
        public bool IsRecording() {
            return m_recording;
        }

        // **********************************************************
        private void RxDataCallback(IAsyncResult res) {
            try {
                IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, kUDP_DATA_PORT);
                byte[] data = m_udpDataClient.EndReceive(res, ref RemoteIpEndPoint);
                if (data.Count() == kDATA_PACKET_SIZE) {
                    m_dataPacketCount++;
                    //m_fsDataOut.Write(data, 0, data.Count());

                    // If the callback is non-null, then create a data packet & call the data callback
                    if (m_dataCallback != null) {
                        Lidar.DataPacket packet = Lidar.ParseBuffer(data);
                        if (packet != null) {
                            m_dataCallback(packet);
                        }
                    }

                }
                m_udpDataClient.BeginReceive(new AsyncCallback(RxDataCallback), null);
            } catch { }
        }

        // **********************************************************
        private void RxGpsCallback(IAsyncResult res) {
            try {
                IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, kUDP_GPS_PORT);
                byte[] data = m_udpGpsClient.EndReceive(res, ref RemoteIpEndPoint);
                if (data.Count() == kGPS_PACKET_SIZE) {
                    m_gpsPacketCount++;
                    //m_fsGpsOut.Write(data, 0, data.Count());

                    // If the callback is non-null, then create a data packet & call the data callback
                    if (m_gpsCallback != null) {
                        GPS.DataPacket packet = GPS.ParseBuffer(data);
                        if (packet != null) {
                            m_gpsCallback(packet);
                        }
                    }

                }
                m_udpGpsClient.BeginReceive(new AsyncCallback(RxGpsCallback), null);
            } catch { }
        }

    }

}