﻿//
// ************************************************************************
// Copyright (c) 2016 Hydro-Technologies All rights reserved
// This source file is the exclusive property of Hydro-Technologies and may
// not be copied or used without prior written permission.
// ************************************************************************
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HT_VLP16_Lib {

    public class GPS {

        /// <summary>
        /// Convert a GPS week plus time of week to UTC (DateTime)
        /// </summary>
        /// <param name="week">GPS week number</param>
        /// <param name="tow">Time of week in seconds (GPS tow is in milli-seconds)</param>
        /// <returns>The UTC time for the given information</returns>
        public static DateTime GetUtcTime(UInt32 week, double tow) {
            DateTime datum = new DateTime(1980, 1, 6, 0, 0, 0);
            DateTime dtWeek;
            DateTime time = new DateTime(1980, 1, 6, 0, 0, 0);
            dtWeek = datum.AddDays(week * 7);
            time = dtWeek.AddSeconds(tow);
            return time;
        }

        // ******************************************************************
        /// <summary>
        /// The GPS data packet contains a timestamp from the LIDAR as well as the
        /// data parsed from the GPS data stream. 
        /// </summary>
        public class DataPacket {

            public DateTime GetGPSTime() {
                DateTime date = DateTime.UtcNow;
                if (_year > 0) {
                    date = new DateTime((_year + 2000), _month, _day, _hour, _minute, _second);
                }
                DateTime time = date;
                if (_milliSecond > 0) {
                    time = date.AddMilliseconds(_milliSecond);
                }
                return time;
            }

            public DateTime GetLidarGPSTime() {
                DateTime date = DateTime.UtcNow;
                if (_year > 0) {
                    date = new DateTime((_year + 2000), _month, _day, _hour, 0, 0);
                }
                DateTime time = date.AddTicks((long)((_timeStamp) * 10000000.0));
                return time;
            }


            public DataPacket() {
                _timeStamp = 0;
                _dtTime = DateTime.UtcNow;
                _valid = false;
                _hour = 0;
                _minute = 0;
                _second = 0;
                _milliSecond = 0;
                _latitude = 0;
                _latNorth = false;
                _longitude = 0;
                _longEast = false;
                _speed = 0;
                _trueCourse = 0;
                _day = 0;
                _month = 0;
                _year = 0;
                _variation = 0;
                _varEast = false;
                _checksum = 0;
            }

            public double _timeStamp;
            public DateTime _dtTime;
            public bool _valid;
            public UInt16 _hour;
            public UInt16 _minute;
            public UInt16 _second;
            public UInt16 _milliSecond;
            public double _latitude;
            public bool _latNorth;
            public double _longitude;
            public bool _longEast;
            public double _speed;
            public double _trueCourse;
            public UInt16 _day;
            public UInt16 _month;
            public UInt16 _year;
            public double _variation;
            public bool _varEast;
            public UInt32 _checksum;
        }


        // ******************************************************************
        public class PositionEntry {

            public PositionEntry(double timeStamp) {
                _timeStamp = timeStamp;
                _week = 0;
                _rtkf = 0;
                _hour = 0;
                _minute = 0;
                _second = 0;
                _milliSecond = 0;
                _latitude = -999;
                _longitude = -999;
                _altitude = -999;
                _numSats = 0;
            }

            public PositionEntry(DataPacket packet) {
                _timeStamp = packet._timeStamp;
                _hour = packet._hour;
                _minute = packet._minute;
                _second = packet._second;
                _milliSecond = packet._milliSecond;
                _latitude = packet._latitude;
                _longitude = packet._longitude;
                _altitude = -999;
            }

            public byte _rtkf = 0;
            public UInt16 _week = 0;
            public double _tow = 0;
            public double _timeStamp = 0;
            public UInt16 _hour = 0;
            public UInt16 _minute = 0;
            public UInt16 _second = 0;
            public UInt16 _milliSecond = 0;
            public double _latitude = -999;
            public double _longitude = -999;
            public double _altitude = -999;
            public byte _numSats = 0;
            public DateTime _utcTime = new DateTime(1980, 1, 6, 0, 0, 0);

            public static double GetSecondInHour(DateTime utcTime) {
                double sec = utcTime.Minute * 60;
                sec += utcTime.Second;
                sec += (utcTime.Millisecond / 1000.0);
                return sec;
            }

            public double GetSecondInHour() {
                return GetSecondInHour(_utcTime);
            }
        };


        // ******************************************************************
        /// <summary>
        /// Attempt to parse the given data buffer.
        /// </summary>
        /// <param name="data">The data buffer to attempt to parse</param>
        /// <returns>A GPS data packet if the buffer contains valid data, null if not</returns>
        static public DataPacket ParseBuffer(byte[] data) {
            if (data.Count() != 512) {
                return null;
            }
            try {
                DataPacket packet = new DataPacket();

                // Read the timestamp from the LIDAR system.
                byte b1 = data[198];
                byte b2 = data[199];
                byte b3 = data[200];
                byte b4 = data[201];
                UInt32 dwts = (UInt32)(b4 << 24);
                dwts |= (UInt32)(b3 << 16);
                dwts |= (UInt32)(b2 << 8);
                dwts |= (UInt32)(b1);
                packet._timeStamp = (double)dwts / 1000000.0;

                // Read in the GPS string (72 bytes)
                String strGPS = "";
                for (int j = 0; j < 72; j++) {
                    strGPS += Convert.ToChar(data[206 + j]);
                }

                //strGPS = "$GPRMC,220516,A,5133.82,N,00042.24,W,173.8,231.8,130694,004.2,W*70";
                //strGPS = "$GPRMC,123519,A,4807.038,N,01131.000,E,022.4,084.4,230394,003.1,W * 6A";
                //strGPS = "$GPRMC,081836,A,3751.65,S,14507.36,E,000.0,360.0,130998,011.3,E * 62";
                //strGPS = "$GPRMC,225446    ,A,4916.45  ,N,12311.12  ,W,000.5,054.7, 191194,020.3,E * 68";
                //strGPS = "$GPRMC,092750.000,A,5321.6802,N,00630.3372,W,0.02 ,31.66, 280511,,,A*43";

                //strGPS = "$GPRMC,212158,A,4028.0226,N,10453.1177,W,000.0,170.2,181016,008.6,E,D*0F"

                // Attempt to parse the GPS data string
                String strNum = "";
                String[] tokens = strGPS.Split(',');
                String[] subTokens;
                if ((tokens.Length >= 12) &&
                    (tokens[1].Length >= 6) && // the GPS time might include milli-seconds
                    (tokens[9].Length == 6)) { // the GPS date stamp must be exactly 6 characters long
                                               // tokens[0] => $GPRMC

                    // Time Stamp
                    strNum = tokens[1].Substring(0, 2);
                    packet._hour = Convert.ToUInt16(strNum);
                    strNum = tokens[1].Substring(2, 2);
                    packet._minute = Convert.ToUInt16(strNum);
                    strNum = tokens[1].Substring(4, 2);
                    packet._second = Convert.ToUInt16(strNum);
                    subTokens = tokens[1].Split('.');
                    if (subTokens.Length == 2) {
                        packet._milliSecond = Convert.ToUInt16(subTokens[1]);
                    }

                    packet._valid = tokens[2].Contains("A");
                    packet._latitude = Convert.ToDouble(tokens[3]);
                    packet._latNorth = tokens[4].Contains("N");
                    packet._longitude = Convert.ToDouble(tokens[5]);
                    packet._longEast = !tokens[6].Contains("W");
                    if (tokens[7].Length > 0) {
                        packet._speed = Convert.ToDouble(tokens[7]);
                    }
                    if (tokens[8].Length > 0) {
                        packet._trueCourse = Convert.ToDouble(tokens[8]);
                    }

                    packet._latitude = ConvertRawPosition(packet._latitude, packet._latNorth);
                    packet._longitude = ConvertRawPosition(packet._longitude, packet._longEast);

                    // Date Stamp
                    strNum = tokens[9].Substring(0, 2);
                    packet._day = Convert.ToUInt16(strNum);
                    strNum = tokens[9].Substring(2, 2);
                    packet._month = Convert.ToUInt16(strNum);
                    strNum = tokens[9].Substring(4, 2);
                    packet._year = Convert.ToUInt16(strNum);

                    packet._variation = Convert.ToDouble(tokens[10]);
                    packet._varEast = !tokens[11].Contains("W");
                    // NOTE: tokens[11] also contains the checksum which might contain an 'E'

                    DateTime dtUtc = new DateTime(2000 + packet._year, packet._month, packet._day, packet._hour, 0, 0);
                    packet._dtTime = dtUtc.AddTicks((long)(packet._timeStamp * 10000000.0));

                    subTokens = tokens[11].Split('*');
                    if (subTokens.Length == 2) {
                        packet._checksum = (UInt32)int.Parse(subTokens[1], System.Globalization.NumberStyles.HexNumber);
                    }
                    return packet;
                }
                return null;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return null;
        }


        // ******************************************************************
        static private double ConvertRawPosition(double pos, bool NE) {
            double degWhole = (double)((int)(pos / 100));
            double degDec = (pos - degWhole * 100) / 60;
            double deg = degWhole + degDec;
            if (!NE) {
                deg = deg * -1.0;
            }
            return deg;
        }


        // ******************************************************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strGPS"></param>
        /// <returns></returns>
        static public DataPacket ParseString_GPRMC(String strGPS) {
            if (!strGPS.StartsWith("$GPRMC")) {
                return null;
            }

            try {
                DataPacket packet = new DataPacket();

                //strGPS = "$GPRMC,231248,A,4028.0260,N,10453.1161,W,000.0,174.7,100816,008.6,E,D*09"

                //strGPS = "$GPRMC,220516,A,5133.82,N,00042.24,W,173.8,231.8,130694,004.2,W*70";
                //strGPS = "$GPRMC,123519,A,4807.038,N,01131.000,E,022.4,084.4,230394,003.1,W * 6A";
                //strGPS = "$GPRMC,081836,A,3751.65,S,14507.36,E,000.0,360.0,130998,011.3,E * 62";
                //strGPS = "$GPRMC,225446    ,A,4916.45  ,N,12311.12  ,W,000.5,054.7, 191194,020.3,E * 68";
                //strGPS = "$GPRMC,092750.000,A,5321.6802,N,00630.3372,W,0.02 ,31.66, 280511,,,A*43";

                // Attempt to parse the GPS data string
                String strNum = "";
                String[] tokens = strGPS.Split(',');
                String[] subTokens;
                //if (tokens.Length > 1) {
                //    System.Windows.Forms.MessageBox.Show(strGPS, "String Found");
                // }
                if ((tokens.Length >= 12) &&
                    (tokens[1].Length >= 6) && // the GPS time might include milli-seconds
                    (tokens[9].Length == 6)) { // the GPS date stamp must be exactly 6 characters long
                                               // tokens[0] => $GPRMC

                    // Time Stamp
                    strNum = tokens[1].Substring(0, 2);
                    packet._hour = Convert.ToUInt16(strNum);
                    strNum = tokens[1].Substring(2, 2);
                    packet._minute = Convert.ToUInt16(strNum);
                    strNum = tokens[1].Substring(4, 2);
                    packet._second = Convert.ToUInt16(strNum);
                    subTokens = tokens[1].Split('.');
                    if (subTokens.Length == 2) {
                        packet._milliSecond = Convert.ToUInt16(subTokens[1]);
                    }
                    if (packet._milliSecond >= 1000) {
                        packet._second++;
                        packet._milliSecond = 0;
                    }
                    if (packet._second == 60) {
                        packet._minute++;
                        packet._second = 0;
                    }
                    if (packet._minute == 60) {
                        packet._hour++;
                        packet._minute = 0;
                    }
                    if (packet._hour > 24) {
                        packet._hour = 0;
                    }

                    packet._valid = tokens[2].Contains("A");
                    packet._latitude = Convert.ToDouble(tokens[3]);
                    packet._latNorth = tokens[4].Contains("N");
                    packet._longitude = Convert.ToDouble(tokens[5]);
                    packet._longEast = !tokens[6].Contains("W");

                    packet._latitude = ConvertRawPosition(packet._latitude, packet._latNorth);
                    packet._longitude = ConvertRawPosition(packet._longitude, packet._longEast);

                    double.TryParse(tokens[7], out packet._speed);
                    double.TryParse(tokens[8], out packet._trueCourse);

                    // Date Stamp
                    strNum = tokens[9].Substring(0, 2);
                    packet._day = Convert.ToUInt16(strNum);
                    strNum = tokens[9].Substring(2, 2);
                    packet._month = Convert.ToUInt16(strNum);
                    strNum = tokens[9].Substring(4, 2);
                    packet._year = Convert.ToUInt16(strNum);

                    double.TryParse(tokens[10], out packet._variation);

                    packet._varEast = !tokens[11].Contains("W");
                    // NOTE: tokens[11] also contains the checksum which might contain an 'E'

                    subTokens = tokens[11].Split('*');
                    if (subTokens.Length == 2) {
                        packet._checksum = (UInt32)int.Parse(subTokens[1], System.Globalization.NumberStyles.HexNumber);
                    }
                    return packet;
                }
                return null;
            } catch {
            }
            return null;
        }


        /// *****************************************************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsInput"></param>
        /// <returns></returns>
        static public List<DataPacket> ParseBinaryFile(FileStream fsInput, System.Windows.Forms.TextBox txtBox = null) {
            List<DataPacket> fileData = new List<DataPacket>();
            Int64 len = fsInput.Length;
            byte[] data = new byte[512];
            double lastTs = 0;
            double tsAdd = 0;
            int count = 0;
            int lastSecond = -1;
            bool hourTransition = false;
            bool add3599 = false;
            do {
                fsInput.Read(data, 0, 512);
                try {
                    DataPacket packet = ParseBuffer(data);
                    if (packet != null) {

                        // The Lidar timestamps get funny around the transition from one hour to the next
                        //
                        // When the the Lidar first gets GPS lock, the time may jump unexpectedly
                        //
                        // Sometimes they roll over as expected, they go from 3599.xxx to 0.xxx
                        // 
                        // Sometimes they stop at 3598.xxx then the next values are less than 1.0
                        // This appears to always be followed by values > 3600
                        //
                        if ((!hourTransition) && (packet._timeStamp >= 3598) && (packet._timeStamp < 3600)) {
                            hourTransition = true;
                        }
                        if (hourTransition) {
                            if ((packet._timeStamp < 1.0) && (lastSecond == 3598)) {
                                // When this happens we need to add 3599.0 to
                                // all entries until the real rollover occurs
                                add3599 = true;
                            }
                        }
                        // When we see a value in excess 0f 3600.0 we have rolled over the hour
                        if (packet._timeStamp >= 3600) {
                            packet._timeStamp -= 3600.0;
                            add3599 = false;
                        }
                        if (add3599) {
                            packet._timeStamp += 3599.0;
                        }

                        if (lastTs > (packet._timeStamp + tsAdd)) {
                            double deltaT = lastTs - (packet._timeStamp + tsAdd);
                            if ((hourTransition) || (deltaT > 3575)) {
                                tsAdd += 3600;
                                hourTransition = false;
                                add3599 = false;
                            }
                        }
                        lastSecond = (int)packet._timeStamp;

                        DateTime dtUtc = new DateTime(2000 + packet._year, packet._month, packet._day, packet._hour, 0, 0);
                        packet._dtTime = dtUtc.AddTicks((long)(packet._timeStamp * 10000000.0));

                        packet._timeStamp += tsAdd;
                        lastTs = packet._timeStamp;
                        fileData.Add(packet);
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
                if (txtBox != null) {
                    if (count++ >= 250) {
                        count = 0;
                        double pcnt = ((double)fsInput.Position / (double)len) * 100;
                        txtBox.Text = pcnt.ToString("N1") + " %";
                        System.Windows.Forms.Application.DoEvents();
                    }
                }

            } while (fsInput.Position < len);

            // The timestamp and the GPS hour don't roll over at exactly the same time
            // To account for this we go through the packets and look for 
            // a missmatch between date time and timestamps
            for (int j = 1; j < fileData.Count; j++) {
                DataPacket packet1 = fileData[j-1];
                DataPacket packet2 = fileData[j];
                if (packet2._dtTime < packet1._dtTime) {
                    if (packet2._timeStamp > packet1._timeStamp) {
                        fileData[j]._dtTime = fileData[j]._dtTime.AddHours(1);
                    }
                }
            }
            return fileData;
        }


        /// *****************************************************************
        static public void WriteTimeStampData(FileStream fsInput, StreamWriter sw, System.Windows.Forms.TextBox txtBox = null) {

            Int64 len = fsInput.Length;
            byte[] data = new byte[512];
            int count = 0;

            double lastTs = 0;
            double tsAdd = 0;
            bool timeShift = false;
            int lastSecond = -1;
            bool hourTransition = false;
            bool add3599 = false;

            sw.WriteLine("Timestamp,gHour,gMinute,gSecond,Hour,Minute,Second,Delta,TimeShift");

            do {
                fsInput.Read(data, 0, 512);
                try {
                    DataPacket packet = ParseBuffer(data);
                    if (packet != null) {

                        int second = (int)packet._timeStamp;
                        int testSec = (packet._minute * 60) + packet._second;
                        int delta = second - testSec;

                        timeShift = false;

                        // The Lidar timestamps get funny around the transition from one hour to the next
                        //
                        // When the the Lidar first gets GPS lock, the time may jump unexpectedly
                        //
                        // Sometimes they roll over as expected, they go from 3599.xxx to 0.xxx
                        // 
                        // Sometimes they stop at 3598.xxx then the next values are less than 1.0
                        // This appears to always be followed by values > 3600
                        //
                        if ((!hourTransition) && (packet._timeStamp >= 3598) && (packet._timeStamp < 3600)) {
                            hourTransition = true;
                        }
                        if (hourTransition) {
                            if ((packet._timeStamp < 1.0) && (lastSecond == 3598)) {
                                // When this happens we need to add 3599.0 to
                                // all entries until the real rollover occurs
                                add3599 = true;
                            }
                        }
                        // When we see a value in excess 0f 3600.0 we have rolled over the hour
                        if (packet._timeStamp >= 3600) {
                            packet._timeStamp -= 3600.0;
                            add3599 = false;
                        }
                        if (add3599) {
                            packet._timeStamp += 3599.0;
                        }

                        if (lastTs > (packet._timeStamp + tsAdd)) {
                            double deltaT = lastTs - (packet._timeStamp + tsAdd);
                            if ((hourTransition) || (deltaT > 3575)) {
                                tsAdd += 3600;
                                hourTransition = false;
                                add3599 = false;
                                timeShift = true;
                            } else {
                                timeShift = true;
                            }
                        }

                        lastSecond = (int)packet._timeStamp;

                        DateTime dtUtc = new DateTime(2000 + packet._year, packet._month, packet._day, packet._hour, 0, 0);
                        packet._dtTime = dtUtc.AddTicks((long)(packet._timeStamp * 10000000.0));

                        packet._timeStamp += tsAdd;
                        lastTs = packet._timeStamp;

                        String str = packet._timeStamp.ToString() + ",";
                        str += packet._hour.ToString() + ",";
                        str += packet._minute.ToString() + ",";
                        str += packet._second.ToString() + ",";
                        str += packet._dtTime.Hour.ToString() + ",";
                        str += packet._dtTime.Minute.ToString() + ",";
                        str += packet._dtTime.Second.ToString() + ",";
                        str += delta.ToString() + ",";
                        str += timeShift.ToString();
                        if (timeShift) {
                            str += ",****";
                        }
                        sw.WriteLine(str);
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
                if (txtBox != null) {
                    if (count++ >= 250) {
                        count = 0;
                        double pcnt = ((double)fsInput.Position / (double)len) * 100;
                        txtBox.Text = pcnt.ToString("N1") + " %";
                        System.Windows.Forms.Application.DoEvents();
                    }
                }

            } while (fsInput.Position < len);
            return;
        }



    }
}