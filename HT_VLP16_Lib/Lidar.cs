﻿//
// ************************************************************************
// Copyright (c) 2016 Hydro-Technologies All rights reserved
// This source file is the exclusive property of Hydro-Technologies and may
// not be copied or used without prior written permission.
// ************************************************************************
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using CSML;

namespace HT_VLP16_Lib {

    public class Lidar {


        public static double DegToRad(double degAngle) { return (degAngle * (Math.PI / 180.0)); }
        public static double RadToDeg(double radAngle) { return (radAngle * (180.0 / Math.PI)); }


        // **********************************************************
        public class ChanData {
            public UInt16 _distance = 0;
            public byte _reflect = 0;

            // **********************************************************
            public void WriteToFile(BinaryWriter bw) {
                bw.Write(_distance);
                bw.Write(_reflect);
            }

            // **********************************************************
            public bool ReadFromFile(BinaryReader br) {
                try {
                    _distance = br.ReadUInt16();
                    _reflect = br.ReadByte();
                    return true;
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
                return false;
            }

        };

        // **********************************************************
        /// <summary>
        /// Each Data Block consists of 32 entries logically broken into an "upper bank"
        /// and a "lower bank" of 16 entries each (one entry per laser). The transmitted 
        /// data contains the angle of rotation (azimuth) only for the upper block (angle 1). 
        /// The azimuth for the lower bank (angle 2) must be interpolated by looking at the 
        /// "upper banks" for two consecutive Data Blocks.
        /// </summary>
        public class DataBlock {

            // **********************************************************
            public DataBlock() {
                _upperAngle = 0;
                _lowerAngle = 0;
                _upperChanData = new ChanData[16];
                _lowerChanData = new ChanData[16];
                for (int j = 0; j < 16; j++) {
                    _upperChanData[j] = new ChanData();
                    _lowerChanData[j] = new ChanData();
                }
            }

            // **********************************************************
            public void WriteToFile(BinaryWriter bw) {
                bw.Write(_upperAngle);
                bw.Write(_lowerAngle);
                for (int j = 0; j < 16; j++) {
                    _upperChanData[j].WriteToFile(bw);
                    _lowerChanData[j].WriteToFile(bw);
                }
            }

            // **********************************************************
            public bool ReadFromFile(BinaryReader br) {
                try {
                    _upperAngle = br.ReadSingle();
                    _lowerAngle = br.ReadSingle();
                    for (int j = 0; j < 16; j++) {
                        if (!_upperChanData[j].ReadFromFile(br)) {
                            return false;
                        }
                        if (!_lowerChanData[j].ReadFromFile(br)) {
                            return false;
                        }
                    }
                    return true;
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
                return false;
            }

            public float _upperAngle;
            public float _lowerAngle;
            public ChanData[] _upperChanData;
            public ChanData[] _lowerChanData;
        };

        // **********************************************************
        /// <summary>
        /// A Data Packet is a single broadcast of data from the LIDAR.
        /// Each Data Packet consists of 12 data blocks, a timestamp,
        /// a byte to indicate the sensor type and another to indicate 
        /// the return mode
        /// </summary>
        public class DataPacket {

            // **********************************************************
            public DateTime GetLidarGPSTime() {
                DateTime curLidarT = new DateTime(_dtUTC.Year, _dtUTC.Month, _dtUTC.Day, _dtUTC.Hour, 0, 0);
                curLidarT = curLidarT.AddTicks((long)((_timeStamp) * 10000000.0));
                return curLidarT;
            }

            // **********************************************************
            public DataPacket() {
                _dtUTC = DateTime.UtcNow;
                _timeStamp = 0;
                _returnMode = 0;
                _sensorType = 0;
                _dataBlock = new DataBlock[12];
                for (int j = 0; j < 12; j++) {
                    _dataBlock[j] = new DataBlock();
                }
            }

            // **********************************************************
            public void WriteToFile(BinaryWriter bw) {
                bw.Write(_dtUTC.Ticks);
                bw.Write(_timeStamp);
                bw.Write(_returnMode);
                bw.Write(_sensorType);
                for (int j = 0; j < 12; j++) {
                    _dataBlock[j].WriteToFile(bw);
                }
            }

            // **********************************************************
            public bool ReadFromFile(BinaryReader br) {
                try {
                    _dtUTC = new DateTime(br.ReadInt64());
                    _timeStamp = br.ReadDouble();
                    _returnMode = br.ReadByte();
                    _sensorType = br.ReadByte();
                    for (int j = 0; j < 12; j++) {
                        if (!_dataBlock[j].ReadFromFile(br)) {
                            return false;
                        }
                    }
                    return true;
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
                return false;
            }

            public DateTime _dtUTC;
            public double _timeStamp;
            public byte _returnMode;
            public byte _sensorType;
            public DataBlock[] _dataBlock;
        };


        // **********************************************************
        public class PointXYZ {
            public byte reflect = 0;
            public double X = 0;
            public double Y = 0;
            public double Z = 0;
            public byte laser = 0;

            public String GetXYZStr() {
                if ((double.IsNaN(X)) || (double.IsNaN(Y)) || (double.IsNaN(Z))) {
                    X = 0;
                    Y = 0;
                    Z = 0;
                }
                return X.ToString() + " " + Y.ToString() + " " + Z.ToString();
            }
            public String GetXYZReflStr() { return X.ToString() + " " + Y.ToString() + " " + Z.ToString() + " " + reflect.ToString(); }
        }

        public class PointXYZRGB : PointXYZ {
            public byte R = 0;
            public byte G = 0;
            public byte B = 0;
            public String GetXYZRGBStr() { return GetXYZStr() + " " + R.ToString() + " " + G.ToString() + " " + B.ToString(); }
        }


        static private double[] ElevationAngle = new double[16];
        static private double[] ElevationAngleCos = new double[16];
        static private double[] ElevationAngleSin = new double[16];
        static private bool _initialized = false;

        public static void Initialize() {
            ElevationAngle[0] = -15;
            ElevationAngle[1] = 1;
            ElevationAngle[2] = -13;
            ElevationAngle[3] = 3;

            ElevationAngle[4] = -11;
            ElevationAngle[5] = 5;
            ElevationAngle[6] = -9;
            ElevationAngle[7] = 7;

            ElevationAngle[8] = -7;
            ElevationAngle[9] = 9;
            ElevationAngle[10] = -5;
            ElevationAngle[11] = 11;

            ElevationAngle[12] = -3;
            ElevationAngle[13] = 13;
            ElevationAngle[14] = -1;
            ElevationAngle[15] = 15;
            for (int j = 0; j < 16; j++) {
                ElevationAngleCos[j] = Math.Cos((ElevationAngle[j] * (Math.PI / 180)));
                ElevationAngleSin[j] = Math.Sin((ElevationAngle[j] * (Math.PI / 180)));
            }
        }


        // **********************************************************
        public static void SavePLY(String filename, List<Lidar.PointXYZRGB> cloud) {
            int vertexCount = cloud.Count;
            FileStream fsxr = new FileStream(filename, FileMode.Create);
            StreamWriter swxr = new StreamWriter(fsxr);
            swxr.WriteLine("ply");
            swxr.WriteLine("format ascii 1.0");
            swxr.WriteLine("comment Hydro-Tech generated");
            swxr.WriteLine("element vertex " + vertexCount.ToString());
            swxr.WriteLine("property float x");
            swxr.WriteLine("property float y");
            swxr.WriteLine("property float z");
            swxr.WriteLine("property uchar red");
            swxr.WriteLine("property uchar green");
            swxr.WriteLine("property uchar blue");
            swxr.WriteLine("end_header");
            try {
                for (int j = 0; j < vertexCount; j++) {
                    String str = cloud[j].GetXYZRGBStr();
                    swxr.WriteLine(str);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            swxr.Close();
            fsxr.Close();
        }

        // **********************************************************
        public static void SaveXYZRGB(String filename, List<Lidar.PointXYZRGB> cloud) {
            int vertexCount = cloud.Count;
            FileStream fsxr = new FileStream(filename, FileMode.Create);
            StreamWriter swxr = new StreamWriter(fsxr);
            try {
                for (int j = 0; j < vertexCount; j++) {
                    String str = cloud[j].GetXYZRGBStr();
                    swxr.WriteLine(str);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            swxr.Close();
            fsxr.Close();
        }

        // **********************************************************
        public static void SaveXYZ(String filename, List<Lidar.PointXYZRGB> cloud) {
            int vertexCount = cloud.Count;
            FileStream fsxr = new FileStream(filename, FileMode.Create);
            StreamWriter swxr = new StreamWriter(fsxr);
            try {
                for (int j = 0; j < vertexCount; j++) {
                    String str = cloud[j].GetXYZStr();
                    swxr.WriteLine(str);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            swxr.Close();
            fsxr.Close();
        }


        public Lidar() {
            if (!_initialized) {
                Initialize();
            }
        }


        // ******************************************************************
        static public Matrix GetRotationMatrix(double deltaYaw, double deltaPitch, double deltaRoll, bool ypr = true, bool ccw_rot = false) {

            Matrix mat = Matrix.Zeros(3);

            double sinYaw = Math.Sin(deltaYaw);
            double cosYaw = Math.Cos(deltaYaw);
            double sinPitch = Math.Sin(deltaPitch);
            double cosPitch = Math.Cos(deltaPitch);
            //double tanPitch = Math.Tan(deltaPitch);
            double sinRoll = Math.Sin(deltaRoll);
            double cosRoll = Math.Cos(deltaRoll);

            Matrix yawm = Matrix.Zeros(3);
            Matrix pitchm = Matrix.Zeros(3);
            Matrix rollm = Matrix.Zeros(3);

            // Apply Yaw rotation (Z remains constant)
            if (ccw_rot) {
                yawm[1, 1] = new Complex(cosYaw); yawm[1, 2] = new Complex(-1.0 * sinYaw); yawm[1, 3] = new Complex(0.0);
                yawm[2, 1] = new Complex(sinYaw); yawm[2, 2] = new Complex(cosYaw); yawm[2, 3] = new Complex(0.0);
                yawm[3, 1] = new Complex(0.0); yawm[3, 2] = new Complex(0.0); yawm[3, 3] = new Complex(1.0);
            } else {
                yawm[1, 1] = new Complex(cosYaw); yawm[1, 2] = new Complex(sinYaw); yawm[1, 3] = new Complex(0.0);
                yawm[2, 1] = new Complex(-1.0 * sinYaw); yawm[2, 2] = new Complex(cosYaw); yawm[2, 3] = new Complex(0.0);
                yawm[3, 1] = new Complex(0.0); yawm[3, 2] = new Complex(0.0); yawm[3, 3] = new Complex(1.0);
            }

            // Apply Pitch rotation (X remains constant)
            if (ccw_rot) {
                pitchm[1, 1] = new Complex(1.0); pitchm[1, 2] = new Complex(0.0); pitchm[1, 3] = new Complex(0.0);
                pitchm[2, 1] = new Complex(0.0); pitchm[2, 2] = new Complex(cosPitch); pitchm[2, 3] = new Complex(-1.0 * sinPitch);
                pitchm[3, 1] = new Complex(0.0); pitchm[3, 2] = new Complex(sinPitch); pitchm[3, 3] = new Complex(cosPitch);
            } else {
                pitchm[1, 1] = new Complex(1.0); pitchm[1, 2] = new Complex(0.0); pitchm[1, 3] = new Complex(0.0);
                pitchm[2, 1] = new Complex(0.0); pitchm[2, 2] = new Complex(cosPitch); pitchm[2, 3] = new Complex(sinPitch);
                pitchm[3, 1] = new Complex(0.0); pitchm[3, 2] = new Complex(-1.0 * sinPitch); pitchm[3, 3] = new Complex(cosPitch);
            }

            // Apply Roll rotation (Y remains constant)
            if (ccw_rot) {
                rollm[1, 1] = new Complex(cosRoll); rollm[1, 2] = new Complex(0.0); rollm[1, 3] = new Complex(sinRoll);
                rollm[2, 1] = new Complex(0.0); rollm[2, 2] = new Complex(1.0); rollm[2, 3] = new Complex(0.0);
                rollm[3, 1] = new Complex(-1.0 * sinRoll); rollm[3, 2] = new Complex(0.0); rollm[3, 3] = new Complex(cosRoll);
            } else {
                rollm[1, 1] = new Complex(cosRoll); rollm[1, 2] = new Complex(0.0); rollm[1, 3] = new Complex(-1.0 * sinRoll);
                rollm[2, 1] = new Complex(0.0); rollm[2, 2] = new Complex(1.0); rollm[2, 3] = new Complex(0.0);
                rollm[3, 1] = new Complex(sinRoll); rollm[3, 2] = new Complex(0.0); rollm[3, 3] = new Complex(cosRoll);
            }

            if (ypr) {
                mat = yawm * pitchm;
                mat = mat * rollm;
            } else {
                mat = rollm * pitchm;
                mat = mat * yawm;
            }

            return mat;
        }


        // ******************************************************************
        /// <summary>
        /// Convert angle & distance to a point in 3D space.
        /// </summary>
        /// <param name="laserId">ID (index) of laser channel, must be in the range [0, 15]</param>
        /// <param name="angle">The rotational angle (in radians)</param>
        /// <param name="dist">The distance to the point</param>
        /// <returns></returns>
        static public PointXYZ ConvertToPoint(int laserId, double angle, double dist) {
            if ((laserId < 0) || (laserId > 15)) {
                return null;
            }
            if (!_initialized) {
                Initialize();
            }

            PointXYZ pt = new PointXYZ();
            double sina = Math.Sin(angle);
            double cosa = Math.Cos(angle);
            pt.X = (float)(dist * ElevationAngleCos[laserId] * sina);
            pt.Y = (float)(dist * ElevationAngleCos[laserId] * cosa);
            pt.Z = (float)(dist * ElevationAngleSin[laserId]);
            pt.laser = (byte)laserId;
            return pt;
        }


        // ******************************************************************
        static public PointXYZRGB ConvertToPoint(int laserId, double angle, double dist,
            Matrix rotMat, Matrix boreSightMat, Matrix leverArm,
            Matrix pointOffset) {
            PointXYZRGB pt = new PointXYZRGB();
            if ((laserId < 0) || (laserId > 15)) {
                return pt;
            }
            if (!_initialized) {
                Initialize();
            }
            double elev = Lidar.DegToRad(ElevationAngle[laserId]);
            //double eDeg = VPLidar::RadToDeg(elev);

            double sina = Math.Sin(angle);
            double cosa = Math.Cos(angle);

            double sinEl = ElevationAngleSin[laserId];
            double cosEl = ElevationAngleCos[laserId];

            pt.X = (float)(dist * cosEl * sina);
            pt.Y = (float)(dist * cosEl * cosa);
            pt.Z = (float)(dist * sinEl);

            
            Matrix localPt = new Matrix(3, 1);
            localPt[1, 1] = new Complex(pt.X);
            localPt[2, 1] = new Complex(pt.Y);
            localPt[3, 1] = new Complex(pt.Z);

            Matrix tempPt1 = boreSightMat * localPt;
            tempPt1[1, 1] = tempPt1[1, 1] - leverArm[1, 1];
            tempPt1[2, 1] = tempPt1[2, 1] - leverArm[2, 1];
            tempPt1[3, 1] = tempPt1[3, 1] - leverArm[3, 1];

            Matrix tempPt2 = rotMat * tempPt1;
            tempPt2[1, 1] = pointOffset[1, 1] + tempPt2[1, 1];
            tempPt2[2, 1] = pointOffset[2, 1] + tempPt2[2, 1];
            tempPt2[3, 1] = pointOffset[3, 1] + tempPt2[3, 1];

            pt.X = tempPt2[1, 1].Re;
            pt.Y = tempPt2[2, 1].Re;
            pt.Z = tempPt2[3, 1].Re;
            
            if (double.IsNaN(pt.X) || double.IsNaN(pt.Y) || double.IsNaN(pt.Z)) {
                pt.X = 0;
                pt.Y = 0;
                pt.Z = 0;
            }
            pt.R = 128;
            pt.G = 128;
            pt.B = 128;

            pt.laser = (byte)laserId;
            return pt;
        }

        // ******************************************************************
        static public List<PointXYZRGB> GetPoints(DataPacket packet) {
            if (packet == null) {
                return null;
            }

            List<PointXYZRGB> points = new List<PointXYZRGB>();

            for (int blk = 0; blk < 12; blk++) {
                int seqIndex = (blk * 2);
                int dataIndex = 0;
                //double dpTimeOffset;
                //double dpTimeStamp;
                float upperAngle = packet._dataBlock[blk]._upperAngle;
                float lowerAngle = packet._dataBlock[blk]._lowerAngle;

                upperAngle *= (float)(Math.PI / 180);
                lowerAngle *= (float)(Math.PI / 180);

                for (int chan = 0; chan < 16; chan++) {
                    dataIndex = chan;
                    //dpTimeOffset = (55.296 * seqIndex) + (2.304 * dataIndex);
                    //dpTimeStamp = packet._timeStamp + dpTimeOffset;
                    float dist = packet._dataBlock[blk]._upperChanData[chan]._distance * 2;
                    byte refl = packet._dataBlock[blk]._upperChanData[chan]._reflect;
                    if (dist >= 500) {
                        PointXYZ point = ConvertToPoint(dataIndex, upperAngle, dist / 1000);
                        PointXYZRGB pt = new PointXYZRGB();
                        pt.X = point.X;
                        pt.Y = point.Y;
                        pt.Z = point.Z;
                        pt.laser = point.laser;
                        pt.R = refl;
                        pt.G = 64;
                        pt.B = (byte)(255 - refl);
                        //pt.timeStamp = dpTimeStamp;
                        pt.reflect = refl;
                        points.Add(pt);
                    }
                }
                seqIndex++;
                for (int chan = 0; chan < 16; chan++) {
                    dataIndex = chan;
                    //dpTimeOffset = (55.296 * seqIndex) + (2.304 * dataIndex);
                    //dpTimeStamp = packet._timeStamp + dpTimeOffset;
                    float dist = packet._dataBlock[blk]._lowerChanData[chan]._distance * 2;
                    byte refl = packet._dataBlock[blk]._lowerChanData[chan]._reflect;
                    if (dist >= 500) {
                        PointXYZ point = ConvertToPoint(dataIndex, lowerAngle, dist / 1000);
                        PointXYZRGB pt = new PointXYZRGB();
                        pt.X = point.X;
                        pt.Y = point.Y;
                        pt.Z = point.Z;
                        pt.laser = point.laser;
                        pt.R = refl;
                        pt.G = 64;
                        pt.B = (byte)(255 - refl);
                        //pt.timeStamp = dpTimeStamp;
                        pt.reflect = refl;
                        points.Add(pt);
                    }
                }
            }
            return points;
        }


        // ******************************************************************
        static public List<PointXYZRGB> GetPoints(DataPacket packet, Matrix rotMat, Matrix boreSightMat, Matrix leverArm, Matrix pointOffset, double minDist = 0.5, double maxDist = 100.0) {
            if (packet == null) {
                return null;
            }

            List<PointXYZRGB> points = new List<PointXYZRGB>();

            for (int blk = 0; blk < 12; blk++) {
                int seqIndex = (blk * 2);
                int dataIndex = 0;
                //double dpTimeOffset;
                //double dpTimeStamp;
                float upperAngle = packet._dataBlock[blk]._upperAngle;
                float lowerAngle = packet._dataBlock[blk]._lowerAngle;

                upperAngle *= (float)(Math.PI / 180);
                lowerAngle *= (float)(Math.PI / 180);

                for (int chan = 0; chan < 16; chan++) {
                    dataIndex = chan;
                    //dpTimeOffset = (55.296 * seqIndex) + (2.304 * dataIndex);
                    //dpTimeStamp = packet._timeStamp + dpTimeOffset;
                    double dist = packet._dataBlock[blk]._upperChanData[chan]._distance * 2;
                    dist = dist / 1000.0;
                    byte refl = packet._dataBlock[blk]._upperChanData[chan]._reflect;
                    if ((dist >= minDist) && (dist <= maxDist)) {
                        PointXYZRGB point = ConvertToPoint(dataIndex, upperAngle, dist, rotMat, boreSightMat, leverArm, pointOffset);
                        point.R = refl;
                        point.G = 64;
                        point.B = (byte)(255 - refl);
                        point.reflect = refl;
                        if ((point.X != 0) || (point.Y != 0) || (point.Z != 0)) {
                            points.Add(point);
                        }
                    }
                }
                seqIndex++;
                for (int chan = 0; chan < 16; chan++) {
                    dataIndex = chan;
                    //dpTimeOffset = (55.296 * seqIndex) + (2.304 * dataIndex);
                    //dpTimeStamp = packet._timeStamp + dpTimeOffset;
                    double dist = packet._dataBlock[blk]._lowerChanData[chan]._distance * 2;
                    dist = dist / 1000.0;
                    byte refl = packet._dataBlock[blk]._lowerChanData[chan]._reflect;
                    if ((dist >= minDist) && (dist <= maxDist)) {
                        PointXYZRGB point = ConvertToPoint(dataIndex, upperAngle, dist, rotMat, boreSightMat, leverArm, pointOffset);
                        point.R = refl;
                        point.G = 64;
                        point.B = (byte)(255 - refl);
                        point.reflect = refl;
                        if ((point.X != 0) || (point.Y != 0) || (point.Z != 0)) {
                            points.Add(point);
                        }
                    }
                }
            }
            return points;
        }


        // ******************************************************************
        /// <summary>
        /// Read a Data Block from the given buffer at the given offset
        /// </summary>
        /// <param name="pBuff">The buffer containing the block data</param>
        /// <param name="offset">The offset in the buffer where the data begins</param>
        /// <param name="dblock">The data block to be filled out</param>
        /// <returns>True if the data block is filled in properly, False if there are detectable errors</returns>
        static private bool ReadDataBlock(byte[] pBuff, int offset, ref DataBlock dblock) {
            int idx = offset;
            try {
                byte b1 = pBuff[idx++];
                byte b2 = pBuff[idx++];
                byte b3;
                // The first two bytes in each block must be 0xFF and 0xEE
                if ((b1 != 0XFF) || (b2 != 0XEE)) {
                    return false;
                }
                // The next two bytes are the angle (azimuth) for the upper channel data
                // the angle for the lower channel data must be calculated (interpolated)
                // afetr we have read in all the data blocks
                b1 = pBuff[idx++];
                b2 = pBuff[idx++];
                UInt16 waz = (UInt16)((b2 << 8) | (b1));
                float daz = waz;
                daz = daz / 100.0F;
                dblock._upperAngle = daz;

                // Read the "upper" channel data
                for (int j = 0; j < 16; j++) {
                    b1 = pBuff[idx++];
                    b2 = pBuff[idx++];
                    UInt16 wdist = (UInt16)((b2 << 8) | (b1));
                    b3 = pBuff[idx++];
                    dblock._upperChanData[j]._distance = wdist;
                    dblock._upperChanData[j]._reflect = b3;
                }

                // Read the "lower" channel data
                for (int j = 0; j < 16; j++) {
                    b1 = pBuff[idx++];
                    b2 = pBuff[idx++];
                    UInt16 wdist = (UInt16)((b2 << 8) | (b1));
                    b3 = pBuff[idx++];
                    dblock._lowerChanData[j]._distance = wdist;
                    dblock._lowerChanData[j]._reflect = b3;
                }

                return true;
            } catch {
            }
            return false;
        }


        // ******************************************************************
        static public void CalcAzimuth(ref DataPacket packet) {
            if (packet._returnMode == 0x39) {
                // Dual return mode
                float totalRot = 0;
                for (int blk = 0; blk < 10; blk += 2) {
                    float angleBN = packet._dataBlock[blk]._upperAngle;
                    float angleBN1 = packet._dataBlock[blk + 2]._upperAngle;
                    if (angleBN1 < angleBN) {
                        angleBN1 += 360;
                    }
                    float avg = (angleBN1 - angleBN) / 2.0F;
                    float lowerAngle = angleBN + avg;
                    if (lowerAngle > 360) {
                        lowerAngle -= 360;
                    }
                    totalRot += avg;
                    packet._dataBlock[blk]._lowerAngle = lowerAngle;
                    packet._dataBlock[blk + 1]._lowerAngle = lowerAngle;
                }
                float avgRot = totalRot / 5.0F;
                packet._dataBlock[10]._lowerAngle = packet._dataBlock[10]._upperAngle + avgRot;
                packet._dataBlock[11]._lowerAngle = packet._dataBlock[10]._lowerAngle;
            } else {
                // Strongest return or Last return
                // calculate the missing azimuth (rotation) angles
                float totalRot = 0;
                for (int blk = 0; blk < 11; blk++) {
                    float angleBN = packet._dataBlock[blk]._upperAngle;
                    float angleBN1 = packet._dataBlock[blk + 1]._upperAngle;
                    if (angleBN1 < angleBN) {
                        angleBN1 += 360;
                    }
                    float avg = (angleBN1 - angleBN) / 2.0F;
                    float lowerAngle = angleBN + avg;
                    if (lowerAngle > 360) {
                        lowerAngle -= 360;
                    }
                    totalRot += avg;
                    packet._dataBlock[blk]._lowerAngle = lowerAngle;
                }
                float avgRot = totalRot / 11.0F;
                packet._dataBlock[11]._lowerAngle = packet._dataBlock[11]._upperAngle + avgRot;
            }
            return;
        }


        // ******************************************************************
        static public DataPacket ParseBuffer(byte[] pBuff) {

            if (pBuff.Length != 1206) {
                return null;
            }

            DataPacket packet = new DataPacket();
            try {
                for (int j = 0; j < 12; j++) {
                    if (!ReadDataBlock(pBuff, j * 100, ref packet._dataBlock[j])) {
                        return null;
                    }
                }
                byte b1 = pBuff[1200];
                byte b2 = pBuff[1201];
                byte b3 = pBuff[1202];
                byte b4 = pBuff[1203];
                UInt32 dwts = (UInt32)(b4 << 24);
                dwts |= (UInt32)(b3 << 16);
                dwts |= (UInt32)(b2 << 8);
                dwts |= (UInt32)(b1);
                packet._timeStamp = (double)dwts / 1000000.0;

                DateTime utc = new DateTime(packet._dtUTC.Year, packet._dtUTC.Month, packet._dtUTC.Day, packet._dtUTC.Hour, 0, 0);
                packet._dtUTC = utc.AddTicks((long)(packet._timeStamp * 10000000.0));

                packet._returnMode = pBuff[1204];
                packet._sensorType = pBuff[1205];
                CalcAzimuth(ref packet);
                return packet;
            } catch { }
            return null;
        }

        // ******************************************************************
        static public List<DataPacket> ParseBinaryFile(FileStream fsInput) {
            List<DataPacket> fileData = new List<DataPacket>();
            Int64 len = fsInput.Length;
            byte[] data = new byte[1206];
            do {
                fsInput.Read(data, 0, 1206);
                try {
                    DataPacket packet = ParseBuffer(data);
                    if (packet != null) {
                        CalcAzimuth(ref packet);
                        fileData.Add(packet);
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            } while (fsInput.Position < len);
            return fileData;
        }


        // ******************************************************************
        static public void WriteBinaryFile(FileStream fsOutput, List<DataPacket> dataPkts, DateTime runStart, DateTime runEnd) {

            BinaryWriter bwr = new BinaryWriter(fsOutput);

            byte[] blkHdr = new byte[2];
            blkHdr[0] = 0xFF;
            blkHdr[1] = 0xEE;
            foreach (DataPacket pkt in dataPkts) {
                if ((pkt._dtUTC >= runStart) && (pkt._dtUTC <= runEnd)) {
                    TimeSpan ts = pkt._dtUTC - runStart;
                    for (int j = 0; j < 12; j++) {
                        bwr.Write(blkHdr);

                        UInt16 waz = (UInt16)(pkt._dataBlock[j]._upperAngle * 100.0);
                        bwr.Write(waz);

                        // Write the "upper" channel data
                        for (int k = 0; k < 16; k++) {
                            bwr.Write(pkt._dataBlock[j]._upperChanData[k]._distance);
                            bwr.Write(pkt._dataBlock[j]._upperChanData[k]._reflect);
                        }

                        // Read the "lower" channel data
                        for (int k = 0; k < 16; k++) {
                            bwr.Write(pkt._dataBlock[j]._lowerChanData[k]._distance);
                            bwr.Write(pkt._dataBlock[j]._lowerChanData[k]._reflect);
                        }
                    }
                    // Write the timestamp
                    UInt32 dwts = (UInt32)(ts.TotalMilliseconds * 1000);
                    bwr.Write(dwts);

                    // Write the factory bytes
                    bwr.Write(pkt._returnMode);
                    bwr.Write(pkt._sensorType);

                } 
            }
            bwr.Close();
            return;
        }


        // ******************************************************************
        static public void RawToPCAP(FileStream fsInput, FileStream fsOutput) {
            byte[] ws_global_hdr = new byte[24];
            byte[] ws_pkt_hdr = new byte[16];
            byte[] pkt_hdr = new byte[42];
            byte[] data = new byte[1206];
            Int64 len = fsInput.Length;

            // Write a file header data (24 bytes)
            ws_global_hdr[0] = 0xD4;
            ws_global_hdr[1] = 0xC3;
            ws_global_hdr[2] = 0xB2;
            ws_global_hdr[3] = 0xA1;
            ws_global_hdr[4] = 0x02;
            ws_global_hdr[5] = 0x00;
            ws_global_hdr[6] = 0x04;
            ws_global_hdr[7] = 0x00;
            ws_global_hdr[8] = 0x00;
            ws_global_hdr[9] = 0x00;

            ws_global_hdr[10] = 0x00;
            ws_global_hdr[11] = 0x00;
            ws_global_hdr[12] = 0x00;
            ws_global_hdr[13] = 0x00;
            ws_global_hdr[14] = 0x00;
            ws_global_hdr[15] = 0x00;
            ws_global_hdr[16] = 0xFF;
            ws_global_hdr[17] = 0xFF;
            ws_global_hdr[18] = 0x00;
            ws_global_hdr[19] = 0x00;

            ws_global_hdr[20] = 0x01;
            ws_global_hdr[21] = 0x00;
            ws_global_hdr[22] = 0x00;
            ws_global_hdr[23] = 0x00;
            fsOutput.Write(ws_global_hdr, 0, 24);

            ws_pkt_hdr[0] = 0xC9;
            ws_pkt_hdr[1] = 0x05;
            ws_pkt_hdr[2] = 0x61;
            ws_pkt_hdr[3] = 0x54;
            ws_pkt_hdr[4] = 0x95;
            ws_pkt_hdr[5] = 0xDA;

            ws_pkt_hdr[6] = 0x05;
            ws_pkt_hdr[7] = 0x00;

            ws_pkt_hdr[8] = 0xE0;
            ws_pkt_hdr[9] = 0x04;
            ws_pkt_hdr[10] = 0x00;
            ws_pkt_hdr[11] = 0x00;
            ws_pkt_hdr[12] = 0xE0;
            ws_pkt_hdr[13] = 0x04;
            ws_pkt_hdr[14] = 0x00;
            ws_pkt_hdr[15] = 0x00;

            pkt_hdr[0] = 0xFF;
            pkt_hdr[1] = 0xFF;
            pkt_hdr[2] = 0xFF;
            pkt_hdr[3] = 0xFF;
            pkt_hdr[4] = 0xFF;
            pkt_hdr[5] = 0xFF;
            pkt_hdr[6] = 0x60;
            pkt_hdr[7] = 0x76;
            pkt_hdr[8] = 0x88;
            pkt_hdr[9] = 0x00;

            pkt_hdr[10] = 0x00;
            pkt_hdr[11] = 0x00;
            pkt_hdr[12] = 0x08;
            pkt_hdr[13] = 0x00;
            pkt_hdr[14] = 0x45;
            pkt_hdr[15] = 0x00;
            pkt_hdr[16] = 0x04;
            pkt_hdr[17] = 0xD2;
            pkt_hdr[18] = 0x00;
            pkt_hdr[19] = 0x00;

            pkt_hdr[20] = 0x40;
            pkt_hdr[21] = 0x00;
            pkt_hdr[22] = 0xFF;
            pkt_hdr[23] = 0x11;
            pkt_hdr[24] = 0xB4;
            pkt_hdr[25] = 0xAA;
            pkt_hdr[26] = 0xC0;
            pkt_hdr[27] = 0xA8;
            pkt_hdr[28] = 0x01;
            pkt_hdr[29] = 0xC8;

            pkt_hdr[30] = 0xFF;
            pkt_hdr[31] = 0xFF;
            pkt_hdr[32] = 0xFF;
            pkt_hdr[33] = 0xFF;
            pkt_hdr[34] = 0x09;
            pkt_hdr[35] = 0x40;
            pkt_hdr[36] = 0x09;
            pkt_hdr[37] = 0x40;
            pkt_hdr[38] = 0x04;
            pkt_hdr[39] = 0xBE;

            pkt_hdr[40] = 0x00;
            pkt_hdr[41] = 0x00;

            do {
                fsInput.Read(data, 0, 1206);
                try {
                    fsOutput.Write(ws_pkt_hdr, 0, 16);
                    fsOutput.Write(pkt_hdr, 0, 42);
                    fsOutput.Write(data, 0, 1206);
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            } while (fsInput.Position < len);
        }

        // ******************************************************************
        static private int FindPCAPDataBlock(FileStream fsInput) {
            try {
                byte b1 = (byte)fsInput.ReadByte();
                byte b2 = (byte)fsInput.ReadByte();
                int count = 0;
                while ((b1 != 0XFF) || (b2 != 0XEE)) {
                    b1 = b2;
                    b2 = (byte)fsInput.ReadByte();
                    count++;
                    if (count >= 1200) {
                        return count;
                    }
                }
                return count;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return -1;
        }

        // ******************************************************************
        static private bool ReadPCAPDataBlock(FileStream fsInput, ref DataBlock dblock) {
            try {
                byte b1 = (byte)fsInput.ReadByte();
                byte b2 = (byte)fsInput.ReadByte();
                byte b3;
                if ((b1 != 0XFF) || (b2 != 0XEE)) {
                    return false;
                }
                b1 = (byte)fsInput.ReadByte();
                b2 = (byte)fsInput.ReadByte();
                UInt16 waz = (UInt16)((b2 << 8) | (b1));
                float daz = waz;
                daz = daz / 100.0F;
                dblock._upperAngle = daz;

                for (int j = 0; j < 16; j++) {
                    b1 = (byte)fsInput.ReadByte();
                    b2 = (byte)fsInput.ReadByte();
                    UInt16 wdist = (UInt16)((b2 << 8) | (b1));
                    b3 = (byte)fsInput.ReadByte();
                    dblock._upperChanData[j]._distance = wdist;
                    dblock._upperChanData[j]._reflect = b3;
                }
                for (int j = 0; j < 16; j++) {
                    b1 = (byte)fsInput.ReadByte();
                    b2 = (byte)fsInput.ReadByte();
                    UInt16 wdist = (UInt16)((b2 << 8) | (b1));
                    b3 = (byte)fsInput.ReadByte();
                    dblock._lowerChanData[j]._distance = wdist;
                    dblock._lowerChanData[j]._reflect = b3;
                }
                return true;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        // ******************************************************************
        static private bool ReadPCAPDataPacket(FileStream fsInput, ref DataPacket dataPacket) {
            try {
                // Read header
                //byte[] header = new byte[58];
                //fsInput.Read(header, 0, 58);
                // Skip header, find data block
                FindPCAPDataBlock(fsInput);
                fsInput.Seek(-2, SeekOrigin.Current);

                for (int j = 0; j < 12; j++) {
                    if (!ReadPCAPDataBlock(fsInput, ref dataPacket._dataBlock[j])) {
                        return false;
                    }
                }
                byte b1 = (byte)fsInput.ReadByte();
                byte b2 = (byte)fsInput.ReadByte();
                byte b3 = (byte)fsInput.ReadByte();
                byte b4 = (byte)fsInput.ReadByte();
                UInt32 dwts = (UInt32)(b4 << 24);
                dwts |= (UInt32)(b3 << 16);
                dwts |= (UInt32)(b2 << 8);
                dwts |= (UInt32)(b1);
                dataPacket._timeStamp = (int)dwts / 1000.0;
                dataPacket._timeStamp = dataPacket._timeStamp / 1000.0;
                dataPacket._returnMode = (byte)fsInput.ReadByte();
                dataPacket._sensorType = (byte)fsInput.ReadByte();
                return true;
            } catch { }
            return false;
        }

        // ******************************************************************
        static public List<DataPacket> ParsePCAPFile(FileStream fsInput) {
            List<DataPacket> fileData = new List<DataPacket>();
            Int64 len = fsInput.Length;

            FindPCAPDataBlock(fsInput);
            fsInput.Seek(-44, SeekOrigin.Current);

            do {
                try {
                    DataPacket packet = new DataPacket();
                    if (ReadPCAPDataPacket(fsInput, ref packet)) {
                        CalcAzimuth(ref packet);
                        fileData.Add(packet);
                    } else {
                        FindPCAPDataBlock(fsInput);
                        fsInput.Seek(-44, SeekOrigin.Current);
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            } while (fsInput.Position < len);

            return fileData;
        }

        // ******************************************************************
        static public List<DateTime> ParseBinFileTS(FileStream fsInput, DateTime dtTopOfHour, System.Windows.Forms.TextBox txtBox = null) {
            List<DateTime> timeStamps = new List<DateTime>();
            Int64 len = fsInput.Length;
            byte[] data = new byte[1206];
            double lastTs = 0;
            double tsAdd = 0;
            int count = 0;

            int lastSecond = -1;
            bool hourTransition = false;
            bool add3599 = false;

            do {
                fsInput.Read(data, 0, 1206);
                try {
                    DataPacket packet = ParseBuffer(data);
                    if (packet != null) {
                        // The Lidar timestamps get funny around the transition from one hour to the next
                        //
                        // When the the Lidar first gets GPS lock, the time may jump unexpectedly
                        //
                        // Sometimes they roll over as expected, they go from 3599.xxx to 0.xxx
                        // 
                        // Sometimes they stop at 3598.xxx then the next values are less than 1.0
                        // This appears to always be followed by values > 3600
                        //
                        if ((!hourTransition) && (packet._timeStamp >= 3598) && (packet._timeStamp < 3600)) {
                            hourTransition = true;
                        }
                        if (hourTransition) {
                            if ((packet._timeStamp < 1.0) && (lastSecond == 3598)) {
                                // When this happens we need to add 3599.0 to
                                // all entries until the real rollover occurs
                                add3599 = true;
                            }
                        }
                        // When we see a value in excess 0f 3600.0 we have rolled over the hour
                        if (packet._timeStamp >= 3600) {
                            packet._timeStamp -= 3600.0;
                            add3599 = false;
                        }
                        if (add3599) {
                            packet._timeStamp += 3599.0;
                        }

                        if (lastTs > (packet._timeStamp + tsAdd)) {
                            double deltaT = lastTs - (packet._timeStamp + tsAdd);
                            if ((hourTransition) || (deltaT > 3575)) {
                                tsAdd += 3600;
                                hourTransition = false;
                                add3599 = false;
                            }
                        }
                        lastSecond = (int)packet._timeStamp;

                        packet._timeStamp += tsAdd;
                        packet._dtUTC = dtTopOfHour.AddTicks((long)(packet._timeStamp * 10000000.0));
                        lastTs = packet._timeStamp;
                        timeStamps.Add(packet._dtUTC);
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
                if (txtBox != null) {
                    if (count++ >= 250) {
                        count = 0;
                        double pcnt = ((double)fsInput.Position / (double)len) * 100;
                        txtBox.Text = pcnt.ToString("N1") + " %";
                        System.Windows.Forms.Application.DoEvents();
                    }
                }
            } while (fsInput.Position < len);
            return timeStamps;
        }


        // ******************************************************************
        static public List<double> CleanFileTS(FileStream fsInput, FileStream fsOutput, double tsMin, double tsMax, System.Windows.Forms.TextBox txtBox = null) {
            List<double> timeStamps = new List<double>();
            Int64 len = fsInput.Length;
            double lastTs = 0;
            double tsAdd = 0;
            byte[] data = new byte[1206];
            byte bHour = 0;
            int displayIdx = 0;
            DataPacket packet;

            double runT = tsMax - tsMin;
            int lastSecond = -1;
            bool hourTransition = false;
            bool add3599 = false;

            do {
                fsInput.Read(data, 0, 1206);
                if (txtBox != null) {
                    displayIdx++;
                    if (displayIdx >= 100) {
                        displayIdx = 0;
                        double pcnt = (double)fsInput.Position / (double)len;
                        pcnt *= 100;
                        txtBox.Text = pcnt.ToString("N1") + " %";
                        txtBox.Refresh();
                        System.Windows.Forms.Application.DoEvents();
                    }
                }
                try {
                    packet = ParseBuffer(data);
                    if (packet != null) {
                        // The Lidar timestamps get funny around the transition from one hour to the next
                        //
                        // When the the Lidar first gets GPS lock, the time may jump unexpectedly
                        //
                        // Sometimes they roll over as expected, they go from 3599.xxx to 0.xxx
                        // 
                        // Sometimes they stop at 3598.xxx then the next values are less than 1.0
                        // This appears to always be followed by values > 3600
                        //
                        if ((!hourTransition) && (packet._timeStamp >= 3598) && (packet._timeStamp < 3600)) {
                            hourTransition = true;
                        }
                        if (hourTransition) {
                            if ((packet._timeStamp < 1.0) && (lastSecond == 3598)) {
                                // When this happens we need to add 3599.0 to
                                // all entries until the real rollover occurs
                                add3599 = true;
                            }
                        }
                        // When we see a value in excess 0f 3600.0 we have rolled over the hour
                        if (packet._timeStamp >= 3600) {
                            packet._timeStamp -= 3600.0;
                            add3599 = false;
                        }
                        if (add3599) {
                            packet._timeStamp += 3599.0;
                        }

                        if (lastTs > (packet._timeStamp + tsAdd)) {
                            double deltaT = lastTs - (packet._timeStamp + tsAdd);
                            if ((hourTransition) || (deltaT > 3575)) {
                                tsAdd += 3600;
                                hourTransition = false;
                                add3599 = false;
                            }
                        }
                        lastSecond = (int)packet._timeStamp;

                        packet._timeStamp += tsAdd;
                        lastTs = packet._timeStamp;

                        if ((packet._timeStamp >= tsMin) && (packet._timeStamp <= tsMax)) {

                            double newTS = (packet._timeStamp - tsMin);
                            double runningTs = (packet._timeStamp - tsMin);
                            if (newTS >= ((bHour + 1) * 3600)) {
                                bHour++;
                            }
                            if (bHour > 0) {
                                newTS = newTS - (bHour * 3600);
                            }
                            newTS = newTS * 1000000.0;
                            newTS += 0.5;

                            UInt32 dwts = (UInt32)(newTS);
                            data[1200] = (byte)(dwts & 0xFF);
                            data[1201] = (byte)(dwts >> 8);
                            data[1202] = (byte)(dwts >> 16);
                            data[1203] = (byte)(dwts >> 24);
                            data[1205] = bHour;
                            fsOutput.Write(data, 0, 1206);

                            timeStamps.Add(runningTs);

                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            } while (fsInput.Position < len);
            return timeStamps;
        }



    }

}